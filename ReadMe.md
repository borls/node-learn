По скринкастам Ильи Кантора
===========================
Настройка проекта
-----------------
1. Создать пустой проект
1. Подключить плагин nodejs
1. Выставить в настройках `Node.JS and NPM` в поле `edit usage scope`
     * Node.JS global
     * Node.JS v0.12.7 Core Modules
1. Edit configuration... -> Node.JS и выбрать файл, который хотите, чтобы запускался


Разделы
-------
**1.** **Echo-server**

При запросе на http://127.0.0.1/echo?message=Hello выдает значение этого параметра ("Hello")

* **supervisor**

    Для автоматического перестроения страницы  
    `npm i -g supervisor`
    Теперь запускать `supervisor server.js`

* **debugger**
    1. отладчик node debug `node --debug server.js`
    2. через браузер Chrome `--debug-brk + node-inspector`
    3. с помощью IDE

* **logging**
    1. debug `npm i debug` простое использование, мало настроек
    2. winston `npm i winston` можно настроить (цвета, для каких файлов выводить и т.д.)
    3. NODE_DEBUG='http net' node server.js
    
    
**2.**Event-loop
    1. LibUV - библиотека на C для работы с событийным циклом (ввод-вывод, таймеры)
    чтобы не ждать, JS должен выполняться мгновенно, то есть не парсить большие json файлы или не вычислять md5 больших загруженных данных
    
**3.**Long-polling
    