var url = require('url');
var log = require('./log')(module);


module.exports = function (req, res) {
    var urlParsed = url.parse(req.url, true);

    log.info('Got request', req.method, req.url);

    if (req.method === 'GET' && urlParsed.pathname === '/echo' && urlParsed.query.message) {
        var message = urlParsed.query.message;
        log.debug('Echo' + message);
        res.setHeader('Cache-control', 'no-cache');
        res.end(message);
        return;
    }

    log.error('unknown URL');

    res.statusCode = '404';
    res.end('Page not found!!!');

};